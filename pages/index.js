import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import {Box,CardMedia, Grid, Typography} from '@mui/material';
import ReactPlayer from 'react-player'
import { useEffect, useState } from 'react';
import Menu from './menu';
export default function Home() {
  const [domLoaded, setDomLoaded] = useState(false);
  var playing = true;
  var url = '/footage.mp4';
  useEffect(()=>{
    url = '/footage.mp4'
    setDomLoaded(true);
  })
  return (
    <>
      <video autoPlay muted loop src="/footage.mp4" id='myVideo'></video>
      <Box className='content-header'>LOGO </Box>
      <Box sx={{position:'fixed',top:'25%',paddingLeft:{md:'100px',sm:'50px',xs:'50px'}}}>
        <Grid container spacing={0}>
          <Grid item md={12} sm={12} xs={12}>
            <Typography variant="body1" color="initial" sx={{fontSize:{md:'40px',sm:'40px',xs:'30px'},color:'rgba(255, 255, 255, 0.5)'}}>Company Name</Typography>
          </Grid>
          <Grid item md={12} sm={12} xs={12}>
            <Typography variant="body1" color="initial" sx={{fontSize:{md:'30px',sm:'30px',xs:'20px'},color:'rgba(255, 255, 255, 1)'}}>This is demo website for Aura Media Group</Typography>
          </Grid>
        </Grid>
      </Box>
      <Menu/>
    </>
  )
}
