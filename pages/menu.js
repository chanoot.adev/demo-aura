import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import {Box,CardMedia,Container, Grid} from '@mui/material';
import ReactPlayer from 'react-player'
import { useEffect, useState } from 'react';
import HomeIcon from '@mui/icons-material/Home';
import MenuIcon from '@mui/icons-material/Menu';
import ImportContactsIcon from '@mui/icons-material/ImportContacts';
import PrecisionManufacturingIcon from '@mui/icons-material/PrecisionManufacturing';
export default function Menu() {
  const [domLoaded, setDomLoaded] = useState(false);
  const [home, setHome] = useState('Home');
  const [contact, setContact] = useState('Contact');
  const [product, setproduct] = useState('Product');
  var playing = true;
  var url = '/footage.mp4';
  useEffect(()=>{
    url = '/footage.mp4'
    setDomLoaded(true);
  })

  const handleMouseEnter = () => {
    
    setTimeout(function() {
        setHome('Home')
        setContact('Contact')
        setproduct('Product')
      }, 300);
  }

  const handleMouseLeave = () => {
    setTimeout(function() {
        setHome('Home')
        setContact('Contact')
        setproduct('Product')
      }, 0);
  }
  return (
    <>
    <Box className='content-menu' onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave} sx={{display:{md:'block',sm:'none',xs:'none'}}}>
        <Grid container spacing={18} paddingTop={'20px'} >
            <Grid item md={12} sx={{whiteSpace:'nowrap'}}>
                <MenuIcon fontSize={'24px'}/>
             </Grid>
            <Grid item md={12} sx={{whiteSpace:'nowrap'}}>
                <HomeIcon sx={{marginRight:'20px',marginLeft:'30px'}} fontSize={'24px'}/> {home}
            </Grid>
            <Grid item md={12} sx={{whiteSpace:'nowrap',paddingLeft:'20px'}}>
                <ImportContactsIcon sx={{marginRight:'20px',marginLeft:'30px'}} fontSize={'24px'}/> {contact}
            </Grid>
            <Grid item md={12} sx={{whiteSpace:'nowrap'}}>
                <PrecisionManufacturingIcon sx={{marginRight:'20px',marginLeft:'30px'}} fontSize={'24px'}/> {product}
            </Grid>
        </Grid>
    </Box>
    
    </>
  )
}
